import React from 'react';
import {Navbar, Form, FormControl, Nav, Button} from 'react-bootstrap';

class MainNav extends React.Component
{
    handleSearch = (query) => {

        this.props.search(query);

    }
    render()
    {
        return(
            <Navbar fixed="top" bg="dark" variant="dark">
                <Navbar.Brand href="#home">News</Navbar.Brand>
                <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                </Nav>
                <Form inline>
                <FormControl 
                onChange={(e)=> this.handleSearch(e.target.value)}
                type="text"
                 placeholder="Search" 
                 className="mr-sm-2" />
                <Button  variant="outline-info">Search</Button>
                </Form>
        </Navbar>
        )
    }
}

export default MainNav;