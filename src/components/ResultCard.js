import React, {Component} from 'react';
import {Card, Button} from 'react-bootstrap';
import moment from 'moment';

class ResultCard extends Component
{
    render()
    {
        const {article} = this.props;
        return(
            <Card className="m-4">
                <Card.Img variant="top" src={article.urlToImage} />
                <Card.Body>
                    <Card.Title>{article.title}</Card.Title>
                    <Card.Text>
                    {article.description}
                    </Card.Text>
                    <Button target="_blank" href={article.url} variant="primary">Check it out</Button>
        <small className="float-right text-muted">{article.source.name + " - " + moment(article.publishedAt).fromNow()}</small>
                </Card.Body>
                </Card>
        )
    }
}

export default ResultCard;