import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

import './MainNav';
import MainNav from './MainNav';
import NewsApi from '../api/NewsApi';

import ResultCard from './ResultCard';

import {Spinner} from 'react-bootstrap';

class App extends React.Component {

  constructor()
  {
    super();
    this.newsApi = new NewsApi();
  }
  
  state = {
    articles: [],
    isLoading: false
  }

  componentDidMount()
  {
      this.getHeadlines("AU");
  }

  search = (query) =>
  {
    console.log("Searching for: " + query);

    this.setState({isLoading: true, article: []});

    this.newsApi.search(query)
    .then(results => {
      console.log(results);

      this.setState({articles: results.articles, isLoading: false});

    })
    .catch(error => {
      console.log(error);
    });
  }
  
  getHeadlines = (countryCode) =>
  {
    this.setState({isLoading: true});
    this.newsApi.getHeadlines(countryCode)
    .then(result => {
      console.log(result);

      this.setState({articles: result.articles, isLoading: false});

    })
    .catch(error => {
      console.log(error);
    });
  }

  render()
  {
    return (
      <div>
        <MainNav search={this.search}></MainNav>

        {this.state.isLoading && <div style={{paddingTop: "200px"}} className="text-center"><Spinner variant="primary" className="p-4" animation="border"></Spinner></div>}
      
        <div style={{paddingTop: "80px"}}>
                {this.state.articles && this.state.articles.map((article, key)=>{
                  return <ResultCard key={key} article={article}/>
                })}
      </div>

      </div>
    );
  }

}

export default App;
