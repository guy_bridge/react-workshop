
const config = {
    API_KEY: "921eb4588a4b42efb68fbe4897f7fb58",
    EVERYTHING_BASE_URL: "https://newsapi.org/v2/everything",
    HEADLINES_BASE_URL: "https://newsapi.org/v2/top-headlines"
}

export default config;