// News API helper
import config from '../config';

class NewsApi
{
    search(query)
    {
        return new Promise((resolve, reject)=>{
            fetch(config.EVERYTHING_BASE_URL + "?q="+query + "&apiKey="+config.API_KEY)
                .then(response => {
                    return response.json();
                })
                .then( json => {
                    return resolve(json);
                })
                .catch(error => {
                    console.log(error);
                    return reject(error);
                })
        });
    }

    getHeadlines(countryCode)
    {
        return new Promise((resolve, reject)=>{

            fetch(config.HEADLINES_BASE_URL + "?country="+ countryCode +"&apiKey=" + config.API_KEY)
            .then(response => {
               return response.json();
            })
            .then(json => {
                return resolve(json);
            })
            .catch(error => {
                console.log(error);
                return reject(error);
            });
        })
      
    
    }
}

export default NewsApi;